class User {
  constructor(name, password) {
    this.name = name;
    this.password = password;
    this.clases = [];
    this.status = true;
  }
}

function login() {
  if (event.type == "submit")
    event.preventDefault();
  let userName = document.getElementsByClassName("userLogin")[0],
  user = userName.value.toLowerCase(),
  password = document.getElementsByClassName("userLogin")[1],
  msgUser = document.getElementById("userHelp"),
  msgPassword = document.getElementById("passwordHelp");

  if (userName.value && password.value) {
    let users = JSON.parse(localStorage.getItem('users')), u = users.find(u => u.name == user);
    if (u && u.name == user)
    if (u.password == password.value) {
      localStorage.setItem('currentUser', JSON.stringify(u));
      (user == "admin") ? location.href = './adminPage.html' : location.href = './userPage.html';
    }
    else {
      msgPassword.innerHTML = '*Contraseña Incorrecta, si olvidaste tu contraseña comunicate con el Administrador';
      msgUser.innerHTML = "";
      password.value = "";
    }
    else {
      password.value = "";
      msgUser.innerHTML = "*Usuario Inexistente";
      msgPassword.innerHTML = "";
    }
  }
  else {
    (userName.value) ? msgUser.innerHTML = "" : msgUser.innerHTML = "*Este campo es obligatorio";
    (password.value) ? msgPassword.innerHTML = "" : msgPassword.innerHTML = "*Este campo es obligatorio";
  }
}

$(function () {
  if (!JSON.parse(localStorage.getItem('users')))
    localStorage.setItem('users', JSON.stringify([new User("admin", "admin")]));
  
  localStorage.setItem('currentUser', JSON.stringify(new User('', '')));
});