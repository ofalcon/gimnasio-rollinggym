function msg(icon, title, txt = "", position = "center") {
  Swal.fire({
    icon: icon,
    title: title,
    text: txt,
    position: position,
    showConfirmButton: false,
    showClass: {
      popup: 'animated fadeInDown faster'
    },
    hideClass: {
      popup: 'animated fadeOutUp faster'
    },
    timer: 1500
  });
}

function logOut() {
  localStorage.setItem('currentUser', JSON.stringify(''));
  location.href = './index.html';
}

function superponeClase(clase, user) {
  let clases = user.clases; 
  for (let i = 0; i < clases.length; i++) {
    if ((clase.inicio > clases[i].inicio && clase.inicio < clases[i].fin) || 
      clases.fin > clases[i].inicio && clase.inicio < clases[i].fin)
      return true;
  }
  return false;
}

function inscribirClase(id) {
  let clase = JSON.parse(localStorage.getItem('clases')).find(c => c.id == id), 
  users = JSON.parse(localStorage.getItem('users')), user = users.findIndex(u => u.name == JSON.parse(localStorage.getItem('currentUser')).name);
  if (superponeClase(clase, users[user])) {
    msg('error', 'Ya estás inscripto en una clase durante este horario');
  }
  else {
    users[user].clases.push(clase);
    msg('success', 'Listo!', 'Se ha inscripto a la clase', 'top-end');
    localStorage.setItem('users', JSON.stringify(users));
    localStorage.setItem('currentUser', JSON.stringify(users[user]));
  }
  listarClases();
}

function cancelarInscripcion(id) {
  let users = JSON.parse(localStorage.getItem('users')), user = users.findIndex(u => u.name == JSON.parse(localStorage.getItem('currentUser')).name);
  Swal.fire({
    title: `<h2>¿Está seguro que desea cancelar su inscripción a <b class="d-inline-flex text-capitalize">${users[user].clases.find(c => c.id == id).nombre}</b>?</h2>`,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "red",
    confirmButtonText: "Borrar",
    showClass: {
      popup: 'animated fadeInDown faster'
    },
    hideClass: {
      popup: 'animated fadeOutUp faster'
    }
  }).then((result) => {
    if (result.value) {
      users[user].clases.splice(users[user].clases.findIndex(c => c.id == id), 1);
      msg('success', 'Listo!', 'Se canceló la inscripción', 'top-end');
      localStorage.setItem('users', JSON.stringify(users));
      localStorage.setItem('currentUser', JSON.stringify(users[user]));
      listarClases();
    }
  });
}

function tipoClase(clase) {
  if (clase.nombre.includes('Funcional'))
    return 'bg-primary';
  if (clase.nombre.includes('Boxeo'))
    return 'bg-success';
  if (clase.nombre.includes('Kickboxing'))
    return 'bg-info';
  if (clase.nombre.includes('Crossfit'))
    return 'bg-warning';
  if (clase.nombre.includes('Zumba'))
    return 'bg-danger';
  return 'bg-secondary';
}

function listarClases(clases = JSON.parse(localStorage.getItem('clases'))) {
  let tabla1 = "", tabla2 = "", user = JSON.parse(localStorage.getItem('currentUser'));
  clases1 = clases.filter(c => user.clases.findIndex(d => d.id == c.id) < 0 && moment(c.inicio).diff(moment()) > 0 && moment(c.inicio).subtract(1, 'd').hours(0).diff(moment(), 'day') < 1).sort((a, b) => moment(a.inicio) - moment(b.inicio)), 
  clases2 = clases.filter(c => user.clases.findIndex(d => d.id == c.id) > -1).sort((a, b) => moment(b.inicio) - moment(a.inicio));
  for(let i = 0; i < clases1.length; i++) {
    let clase = clases1[i], inicio = moment(clase.inicio), color = tipoClase(clase);
    tabla1 +=
    `<div class="col-12 col-md-4">
      <div class="card text-white ${color} my-3 w-100 d-inline-block border-0 shadow-sm">
        <div class="card-header text-black-50">
          <i class="fas fa-clock mr-2 text-black-50"></i>${inicio.fromNow()}
        </div>
        <div class="card-body px-3 py-3">
          <h4 class="card-title">${clase.nombre.toUpperCase()}</h4>
          <p class="card-subtitle text-break pb-2">${inicio.format("ddd D MMM YYYY")} | ${inicio.format("HH:mm")} a ${moment(clase.fin).format("HH:mm")}</p>
          ${(cantidadInscriptos(clase.id) < clase.cupo) ? 
          `<small class="py-3 text-black-50"">Inscriptos: ${cantidadInscriptos(clase.id)} / ${clase.cupo}</small>`
          : `<h6 class="py-3 border-bottom text-danger">CUPO LLENO</h6>
          <p class="text-muted">${inicio.fromNow()}</p>`}
        </div>
        <button class="btn-block border-0 bg-dark text-white card-footer mx-auto my-auto" onclick="inscribirClase('${clase.id}')"><i class="fas fa-check mr-2"></i>Inscribirme!</button>
      </div>
      </div>`;
  }
  
  for(let i = 0; i < clases2.length; i++) {
    let clase = clases2[i], inicio = moment(clase.inicio);
    tabla2 += 
    `<div class="col-12 col-md-4">
      <div class="card bg-white my-3 w-100 d-inline-block border border-rounded shadow">
        <div class="card-body px-3 py-3">
          <div class="justify-content-start pl-0 pb-3">
            <h4 class="card-title text-primary text-capitalize mb-3">${clase.nombre.toUpperCase()}</h4>
            <span class="py-1"><i class="fas fa-clock ml-auto mr-2 text-black-50"></i>${inicio.fromNow()}</span>
          </div>
          <p class="card-subtitle text-break py-2">${inicio.format("ddd D MMM YYYY")}<br>de ${inicio.format("HH:mm")} a ${moment(clase.fin).format("HH:mm")}</p>
          <small class="pb-3">Inscriptos: ${cantidadInscriptos(clase.id)} / ${clase.cupo}</small>
        </div>
        ${(inicio.diff(moment()) > 0) ? 
        `<button class="btn-block border-0 bg-danger text-white card-footer mx-auto my-auto" onclick="cancelarInscripcion('${clase.id}')">
        <i class="fas fa-times"></i><span class="d-none d-md-inline-flex ml-0 ml-md-2">Cancelar</span></button>` : ''}
        </div>  
      </div>
    </div>`; 
  }
  $("#clasesP").html(tabla1 ? tabla1 : `<h4 class="p-3"><b class="font-weight-bold">No hay clases próximas.</b></h4>`);
  $('#clasesI').html(tabla2 ? tabla2 : `<h6 class="ml-3"><b class="font-weight-light">Todavía no te inscribiste a ninguna clase.</b></h6>`);
}

function cantidadInscriptos(id) {
  let users = JSON.parse(localStorage.getItem('users')), inscriptos = 0;
  for(let i = 0; i < users.length; i++)
    if (users[i].clases.find(clase => clase.id == id)) 
      inscriptos++;
  return inscriptos;
}

function cambiarContraseña() {
  let users = JSON.parse(localStorage.getItem('users')), user = JSON.parse(localStorage.getItem('currentUser')),
  password = $(".editPassModalData"), msg = $('.editPassModalMsg');
  msg[0].innerHTML = '';
  msg[1].innerHTML = '';
  if (password[0].value == user.password) {
    if (password[1].value) {
      if (password[1].value != user.password) {
        user.password = password[1].value;
        users.splice(users.findIndex(u => u.name == user.name), 1, user);
        $('#editPassModal').modal('hide');
        localStorage.setItem('users', JSON.stringify(users));
        localStorage.setItem('currentUser', JSON.stringify(user));
        Swal.fire({
          title: 'Contraseña modificada',
          icon: "success",
          confirmButtonText: "Aceptar",
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        }).then((result) => cargar());        
      }
      else
        msg[1].innerHTML = '*La contraseña nueva no puede ser igual a la anterior';
    }
    else
      msg[1].innerHTML = '*ingrese una contraseña';
  }
  else
    msg[0].innerHTML = '*contraseña incorrecta';
}

function cargar() {
  let currentUser = JSON.parse(localStorage.getItem('currentUser')) || '', title = "Primero debe iniciar sesión como Usuario", txt = "";
  if (!currentUser.status && currentUser.name) {
    title = "Su cuenta se encuentra suspendida";
    txt = "Contacte al administrador";
  }
  if (!currentUser || !currentUser.name || currentUser.name == "admin" || !currentUser.status) {
    Swal.fire({
      title: title,
      text: txt,
      icon: "warning",
      confirmButtonText: "Aceptar",
      showClass: {
        popup: 'animated fadeInDown faster'
      },
      hideClass: {
        popup: 'animated fadeOutUp faster'
      },
      allowOutsideClick: false,
      allowEscapeKey: false
    }).then((result) => logOut());
  }
  else {
    $("#navbarBrand").html(`<i class="fas fa-dumbbell"></i> Rolling Gym - ${currentUser.name}`);
    if(currentUser.name === currentUser.password)
      Swal.fire({
        title: 'Bienvenido a Rolling Gym!',
        text: 'A continuación deberás cambiar tu contraseña',
        icon: 'info',
        confirmButtonText: "Aceptar",
        showClass: {
          popup: 'animated fadeInDown faster'
        },
        hideClass: {
          popup: 'animated fadeOutUp faster'
        }
      }).then((result) => $('#editPassModal').modal('show'));
    else
      listarClases();
  }
}

$(function () {
  cargar();
 
  $('#editPassModal').on('bs.modal.hidden', () => {
    for (let i = 0; i < 2; i++) {
      $('.editPassModalData')[i].value = '';
      $('.editPassModalMsg')[i].innerHTML = '';
    }
  });
});